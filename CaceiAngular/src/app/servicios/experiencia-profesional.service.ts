import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExperienciaProfesionalService {

  url = 'http://localhost:8000/api/experienciaprofesional';

  constructor(private http: HttpClient) { }

  llenarCatalogos(){
  	return this.http.get(this.url)
  }

  insertarExperienciaProfesional(experiencia_profesional: JSON){
  	return this.http.post(this.url,JSON.stringify(experiencia_profesional));
  }

  eliminarExperienciaProfesional(experiencia_profesional: JSON){
  	return this.http.delete(
  		`${this.url}/${experiencia_profesional['id_experiencias_profesionales']}`
  	)
  }
}
