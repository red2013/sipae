import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class InformacionGeneralService {

	url = 'http://localhost:8000/api/informaciongeneral';

  constructor(private http:HttpClient) {}

  insertarInformacionGeneral(profesor: JSON){
  	return this.http.post(this.url,JSON.stringify(profesor),{responseType: 'text'});
  }

}
