import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CursosImpartidosService {

  url = 'http://localhost:8000/api/cursosimpartidos';

  constructor(private http: HttpClient) { }

  insertarCursosImpartidos(cursos_impartidos: JSON){
  	return this.http.post(this.url,JSON.stringify(cursos_impartidos));
  }

  llenarCatalogos() {
  	return this.http.get(this.url);
  }

  eliminarCursosImpartidos(cursos_impartidos: JSON){
  	return this.http.delete(`${this.url}/${cursos_impartidos['id_cursos_impartidos']}`)
  }
}
