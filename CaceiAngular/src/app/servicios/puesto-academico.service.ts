import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PuestoAcademicoService {
  
  url = 'http://localhost:8000/api/puestosacademicos';

  constructor(private http: HttpClient) { }

  llenarCatalogos(){
  	return this.http.get(this.url)
  }

  insertarPuestoAcademico(puesto: JSON){
  	return this.http.post(this.url,JSON.stringify(puesto));
  }

  eliminarPuestoAcademico(puesto: JSON){
  	return this.http.delete(`${this.url}/${puesto['id_puesto_academico']}`)
  }
}
