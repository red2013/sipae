import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable ({
  providedIn: 'root'
})
export class ActualizacionDisciplinarService {

  url = 'http://localhost:8000/api/actualizaciondisciplinar';

  constructor(private http: HttpClient) {}

  insertarActualizacionDisciplinar(actualizacion: JSON) {
  	return this.http.post(this.url,JSON.stringify(actualizacion));
  }

  llenarCatalogos() {
  	return this.http.get(this.url);
  }

  eliminarActulizacionDisciplinar(actualizacion: JSON){
  	return this.http.delete(`${this.url}/${actualizacion['id_actualizacion_disciplinar']}`)
  }
}
