import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParticipacionesPlanEstudiosService {

  url = 'http://localhost:8000/api/participacionesplanestudios';

  constructor(private http: HttpClient) { }

  llenarCatalogos(){
  	return this.http.get(this.url)
  }

  insertarParticipacionesPlanEstudios(participaciones_plan_estudios: JSON){
  	return this.http.post(this.url,JSON.stringify(participaciones_plan_estudios));
  }

  eliminarParticipacionesPlanEstudios(participaciones_plan_estudios: JSON){
  	return this.http.delete(
  		`${this.url}/${participaciones_plan_estudios['id_participacion_plan_estudios']}`
  	)
  }
}
