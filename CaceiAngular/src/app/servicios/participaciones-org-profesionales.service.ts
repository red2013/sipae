import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParticipacionesOrgProfesionalesService {

  url = 'http://localhost:8000/api/participacionesorgprofesionales';

  constructor(private http: HttpClient) { }

  insertarParticipacionesOrgProfesionales(participaciones_org_profesionales: JSON){
  	return this.http.post(this.url,JSON.stringify(participaciones_org_profesionales));
  }

  llenarCatalogos() {
  	return this.http.get(this.url);
  }

  eliminarParticipacionesOrgProfesionales(participaciones_org_profesionales: JSON){
  	return this.http.delete(
  		`${this.url}/${participaciones_org_profesionales['id_participacion_org_profesional']}`
  	)
  }
}
