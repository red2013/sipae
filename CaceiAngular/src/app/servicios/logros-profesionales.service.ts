import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LogrosProfesionalesService {

  url = 'http://localhost:8000/api/logrosprofesionales';

  constructor(private http: HttpClient) { }

  llenarCatalogos(){
  	return this.http.get(this.url)
  }

  insertarLogrosProfesionales(logros_profesionales: JSON){
  	return this.http.post(this.url,JSON.stringify(logros_profesionales));
  }

  eliminarLogrosProfesionales(logros_profesionales: JSON){
  	return this.http.delete(`${this.url}/${logros_profesionales['id_logro_profesional']}`)
  }
}
