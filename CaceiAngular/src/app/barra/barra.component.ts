import { Component, OnInit } from '@angular/core';
import { ExportarRegistrosService } from './../servicios/exportar-registros.service';
import * as FileSaver from 'file-saver';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-barra',
  templateUrl: './barra.component.html',
  styleUrls: ['./barra.component.css']
})

export class BarraComponent implements OnInit {

  constructor(private _service: ExportarRegistrosService, private pipe:DatePipe) {}
  currentDate: number = Date.now();
  ngOnInit(): void {
  }

  exportarExcel(){
  	this._service.descargarExcel().subscribe(
  		datos => 
  			FileSaver.saveAs(datos,
  				this.pipe.transform(this.currentDate,'yyyy-MM-dd')+".xlsx")
  	)
  }

}
