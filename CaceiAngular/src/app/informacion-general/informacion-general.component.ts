import { Component, OnInit } from '@angular/core';

import { InformacionGeneralService } from './../servicios/informacion-general.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-informacion-general',
  templateUrl: './informacion-general.component.html',
  styleUrls: ['./informacion-general.component.css']
})
export class InformacionGeneralComponent implements OnInit {

  constructor(private _servicio:InformacionGeneralService) {}

  ngOnInit(): void {}

  informacion_general: JSON

  guardar(formulario: NgForm) {
  	formulario.reset;
  	this.informacion_general = formulario.value;
  	this.informacion_general["id_usuario"] = 1;
    console.log(this.informacion_general);
    this._servicio.insertarInformacionGeneral(this.informacion_general).
    subscribe(
      (respuesta: any) => {
        alert(respuesta);
      }
    );
    console.log(this.informacion_general);
  }

}
