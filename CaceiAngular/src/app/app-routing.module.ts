import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Componentes que conforman las pantallas
import { InformacionGeneralComponent } from './informacion-general/informacion-general.component';
import { FormacionAcademicaComponent } from './formacion-academica/formacion-academica.component';
import { CapacitacionDocenteComponent } from './capacitacion-docente/capacitacion-docente.component';
import { ActualizacionDisciplinarComponent } from './actualizacion-disciplinar/actualizacion-disciplinar.component';
import { PuestoAcademicoComponent } from './puesto-academico/puesto-academico.component';
import { ProductosAcademicosComponent } from './productos-academicos/productos-academicos.component';
import { ExperienciaProfesionalComponent } from './experiencia-profesional/experiencia-profesional.component';
import { ExperienciaDisenioIngenierilComponent } from './experiencia-disenio-ingenieril/experiencia-disenio-ingenieril.component';
import { LogrosProfesionalesComponent } from './logros-profesionales/logros-profesionales.component';
import { ParticipacionesOrgProfesionalesComponent } from './participaciones-org-profesionales/participaciones-org-profesionales.component';
import { PremiosReconocimientosComponent } from './premios-reconocimientos/premios-reconocimientos.component';
import { ParticipacionesPlanEstudiosComponent } from './participaciones-plan-estudios/participaciones-plan-estudios.component';
import { CursosImpartidosComponent } from './cursos-impartidos/cursos-impartidos.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {path: "", redirectTo: "/login", pathMatch: 'full'},
  {path: "login", component: LoginComponent},
	{path: "informacion-general", component: InformacionGeneralComponent, canActivate: [AuthGuard]},
  {path: "formacion-academica", component: FormacionAcademicaComponent, canActivate: [AuthGuard]},
  {path: "capacitacion-docente", component: CapacitacionDocenteComponent, canActivate: [AuthGuard]},
  {path: "actualizacion-disciplinar", component: ActualizacionDisciplinarComponent, canActivate: [AuthGuard]},
  {path: "puesto-academico", component: PuestoAcademicoComponent, canActivate: [AuthGuard]},
  {path: "productos-academicos", component: ProductosAcademicosComponent, canActivate: [AuthGuard]},
  {path: "experiencia-profesional", component: ExperienciaProfesionalComponent, canActivate: [AuthGuard]},
  {path: "experiencia-disenio-ingenieril", component: ExperienciaDisenioIngenierilComponent, canActivate: [AuthGuard]},
  {path: "logros-profesionales", component: LogrosProfesionalesComponent, canActivate: [AuthGuard]},
  {path: "participaciones-org-profesionales", component: ParticipacionesOrgProfesionalesComponent, canActivate: [AuthGuard]},
  {path: "premios-reconocimientos", component: PremiosReconocimientosComponent, canActivate: [AuthGuard]},
  {path: "participaciones-plan-estudios", component: ParticipacionesPlanEstudiosComponent, canActivate: [AuthGuard]},
  {path: "cursos-impartidos", component: CursosImpartidosComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
export const routingComponents = [InformacionGeneralComponent, 
                                  FormacionAcademicaComponent, 
                                  CapacitacionDocenteComponent, 
                                  ActualizacionDisciplinarComponent, 
                                  PuestoAcademicoComponent, 
                                  ProductosAcademicosComponent, 
                                  ExperienciaProfesionalComponent, 
                                  ExperienciaDisenioIngenierilComponent,
                                  LogrosProfesionalesComponent,
                                  ParticipacionesOrgProfesionalesComponent,
                                  PremiosReconocimientosComponent,
                                  ParticipacionesPlanEstudiosComponent,
                                  CursosImpartidosComponent,
                                  LoginComponent] 