import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable, MatTableDataSource  } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import { PremiosReconocimientosService } from '../servicios/premios-reconocimientos.service';

@Component({
  selector: 'app-premios-reconocimientos',
  templateUrl: './premios-reconocimientos.component.html',
  styleUrls: ['./premios-reconocimientos.component.css'] 
})
export class PremiosReconocimientosComponent implements OnInit {

  modal;
  premios_reconocimientos:JSON;
  registros = null;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  columnas: string[] = ['id', 'descripcion', 'accion']

  constructor(private modalService: NgbModal, private _service: PremiosReconocimientosService) { }

  ngOnInit(): void {
    this.llenarCatalogos()
  }

  borrarRegistro(element){
    this._service.eliminarPremiosReconocimientos(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  llenarCatalogos(){
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator
        console.log(catalogos['registros']);
      }
    )
  }

  agregarRegistro(content) {
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  cerrar(){
  	this.modal.close();
  }

  guardar(formulario:NgForm){
  	formulario.reset;
    this.premios_reconocimientos = formulario.value;
    this.premios_reconocimientos['id_usuario'] = 1;
    this._service.insertarPremiosReconocimientos(this.premios_reconocimientos).subscribe(
      (respuesta: any) => {
        alert(respuesta['mensaje'])
      } 
    )
    this.llenarCatalogos()
  	this.modal.close();
  }

}
