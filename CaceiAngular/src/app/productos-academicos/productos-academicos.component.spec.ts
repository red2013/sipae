import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductosAcademicosComponent } from './productos-academicos.component';

describe('ProductosAcademicosComponent', () => {
  let component: ProductosAcademicosComponent;
  let fixture: ComponentFixture<ProductosAcademicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductosAcademicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductosAcademicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
