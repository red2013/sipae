import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ProductosAcademicosService } from '../servicios/productos-academicos.service';

@Component({
  selector: 'app-productos-academicos',
  templateUrl: './productos-academicos.component.html',
  styleUrls: ['./productos-academicos.component.css']
})
export class ProductosAcademicosComponent implements OnInit {

  modal;
  productos_academicos:JSON;
  registros = null

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  columnas: string[] = ["id", "descripcion", "accion"]

  constructor(private modalService: NgbModal, private _service: ProductosAcademicosService) { }

  ngOnInit(): void {
    this.llenarCatalogos()
  }

  borrarRegistro(element){
    this._service.eliminarProductoAcademico(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  llenarCatalogos(){
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros'])
        this.registros.paginator = this.paginator
        console.log(catalogos['registros'])
      }
    )
  }

  agregarRegistro(content) {
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  cerrar(){
  	this.modal.close();
  }

  guardar(formulario:NgForm){
  	formulario.reset;
    this.productos_academicos = formulario.value;
    this.productos_academicos['id_usuario'] = 1;
    this._service.insertarProductoAcademico(this.productos_academicos).subscribe(
      (respuesta: any) => {
        alert(respuesta.mensaje)
      } 
    );
    this.llenarCatalogos()
  	console.log(this.productos_academicos);
  	this.modal.close();
  }

}
