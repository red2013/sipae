import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ExperienciaDisenioIngenierilService } from '../servicios/experiencia-disenio-ingenieril.service';

@Component ({
  selector: 'app-experiencia-disenio-ingenieril',
  templateUrl: './experiencia-disenio-ingenieril.component.html',
  styleUrls: ['./experiencia-disenio-ingenieril.component.css']
})
export class ExperienciaDisenioIngenierilComponent implements OnInit {

  modal;
  experiencia_disenio_ingenieril:JSON;
  nombres_niveles_experiencia_ingenieril: any = [];
  registros = null

  columnas: string[] = ["id", "organismo", "accion"]

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private modalService: NgbModal, private _service: ExperienciaDisenioIngenierilService) {}

  ngOnInit(): void {
    this.llenarCatalogos();
  }

  borrarRegistro(element){
    this._service.eliminarExperienciaDisenioIngenieril(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  llenarCatalogos() {
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.nombres_niveles_experiencia_ingenieril = catalogos['nombres_niveles_experiencia_ingenieril'];
        this.registros = new MatTableDataSource<JSON>(catalogos['registros'])
        this.registros.paginator = this.paginator
        console.log(catalogos['registros'])
      }
    );
  }

  agregarRegistro(content) {
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  cerrar() {
  	this.modal.close();
  }

  guardar(formulario:NgForm) {
  	formulario.reset;
    this.experiencia_disenio_ingenieril = formulario.value;
    this.experiencia_disenio_ingenieril['id_usuario'] = 1;
    console.log(this.experiencia_disenio_ingenieril);
    this._service.insertarExperienciaDisenioIngenieril(this.experiencia_disenio_ingenieril).subscribe (
      (respuesta: any) => {
        alert(respuesta.mensaje)
      }
    );
    this.llenarCatalogos()
  	this.modal.close();
  }

}
