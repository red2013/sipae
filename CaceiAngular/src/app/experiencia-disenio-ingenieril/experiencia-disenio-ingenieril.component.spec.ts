import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ExperienciaDisenioIngenierilComponent } from './experiencia-disenio-ingenieril.component';

describe('ExperienciaDisenioIngenierilComponent', () => {
  let component: ExperienciaDisenioIngenierilComponent;
  let fixture: ComponentFixture<ExperienciaDisenioIngenierilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperienciaDisenioIngenierilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienciaDisenioIngenierilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
