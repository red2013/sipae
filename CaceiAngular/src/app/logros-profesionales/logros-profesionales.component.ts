import { Component, OnInit, ViewChild  } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable, MatTableDataSource  } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import { LogrosProfesionalesService } from '../servicios/logros-profesionales.service';

@Component({
  selector: 'app-logros-profesionales',
  templateUrl: './logros-profesionales.component.html',
  styleUrls: ['./logros-profesionales.component.css']
})
export class LogrosProfesionalesComponent implements OnInit {

  modal;
  logros_profesionales:JSON;
  registros = null;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  columnas: string[] = ['id', 'descripcion', 'accion']

  constructor(private modalService: NgbModal, private _service: LogrosProfesionalesService) { }

  ngOnInit(): void {
    this.llenarCatalogos()
  }

  borrarRegistro(element){
    this._service.eliminarLogrosProfesionales(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  llenarCatalogos(){
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator
        console.log(catalogos['registros']);
      }
    )
  }

  agregarRegistro(content) {
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  cerrar(){
  	this.modal.close();
  }

  guardar(formulario:NgForm){
  	formulario.reset;
    this.logros_profesionales = formulario.value;
    this.logros_profesionales['id_usuario'] = 1;
    this._service.insertarLogrosProfesionales(this.logros_profesionales).subscribe(
      (respuesta: any) => {
        alert(respuesta['mensaje'])
      } 
    )
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator
        console.log(this.registros);
      }
    )
  	this.modal.close();
  }

}
