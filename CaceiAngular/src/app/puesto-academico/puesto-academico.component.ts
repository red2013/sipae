import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { PuestoAcademicoService } from './../servicios/puesto-academico.service';

@Component({
  selector: 'app-puesto-academico',
  templateUrl: './puesto-academico.component.html',
  styleUrls: ['./puesto-academico.component.css']
})
export class PuestoAcademicoComponent implements OnInit {

  modal;
  puesto:JSON;
  registros = null

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  columnas: string[] = ["id","puesto academico","accion"]

  constructor(private modalService: NgbModal, private _service: PuestoAcademicoService) { }

  ngOnInit(): void {
    this.llenarCatalogos()
  }

  borrarRegistro(element){
    this._service.eliminarPuestoAcademico(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  llenarCatalogos(){
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros'])
        this.registros.paginator = this.paginator
        console.log(catalogos['registros'])
      }
    )
  }

  agregarRegistro(content) {
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  cerrar(){
  	this.modal.close();
  }

  guardar(formulario:NgForm){
  	formulario.reset;
    this.puesto = formulario.value;
    this.puesto['id_usuario'] = 1;
    this._service.insertarPuestoAcademico(this.puesto).subscribe(
      (respuesta: any) => {
        alert(respuesta.mensaje)
      } 
    );
    this._service.llenarCatalogos().subscribe( //actualizamos nuvemente los registros
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros'])
        this.registros.paginator = this.paginator
        console.log(catalogos['registros'])
      }
    )
  	console.log(this.puesto);
  	this.modal.close();
  }

}
