import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PuestoAcademicoComponent } from './puesto-academico.component';

describe('PuestoAcademicoComponent', () => {
  let component: PuestoAcademicoComponent;
  let fixture: ComponentFixture<PuestoAcademicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuestoAcademicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuestoAcademicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
