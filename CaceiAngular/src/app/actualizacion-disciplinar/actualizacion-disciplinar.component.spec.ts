import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActualizacionDisciplinarComponent } from './actualizacion-disciplinar.component';

describe('ActualizacionDisciplinarComponent', () => {
  let component: ActualizacionDisciplinarComponent;
  let fixture: ComponentFixture<ActualizacionDisciplinarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizacionDisciplinarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizacionDisciplinarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
