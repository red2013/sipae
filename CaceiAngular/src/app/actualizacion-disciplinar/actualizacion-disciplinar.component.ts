import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ActualizacionDisciplinarService } from './../servicios/actualizacion-disciplinar.service';

@Component({
  selector: 'app-actualizacion-disciplinar',
  templateUrl: './actualizacion-disciplinar.component.html',
  styleUrls: ['./actualizacion-disciplinar.component.css']
})
export class ActualizacionDisciplinarComponent implements OnInit {

  modal;
  actualizacion:JSON;
  paises: any = [];
  registros = null;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  columnas: string[] = ["id","actualizacion","accion"]

  constructor(private modalService: NgbModal, private _service: ActualizacionDisciplinarService) {}

  ngOnInit(): void {
    this.llenarCatalogos();
  }

  borrarRegistro(element){

  }

  llenarCatalogos() {
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.paises = catalogos['paises'];
        this.registros = new MatTableDataSource<JSON>(catalogos['registros'])
        this.registros.paginator = this.paginator
        console.log(catalogos['registros'])
      }
    );
  }

  agregarRegistro(content) {
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  cerrar(){
  	this.modal.close();
  }

  guardar(formulario:NgForm) {
    formulario.reset;
    this.actualizacion = formulario.value;
    this.actualizacion['id_usuario'] = 1;
    this._service.insertarActualizacionDisciplinar(this.actualizacion).subscribe(
      (respuesta: any) => {
        alert(respuesta.mensaje)
      } 
    );
    this._service.llenarCatalogos().subscribe( //actualizamos nuvemente los registros
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros'])
        this.registros.paginator = this.paginator
        console.log(catalogos['registros'])
      }
    )
  	console.log(this.actualizacion);
  	this.modal.close();
  }

}
