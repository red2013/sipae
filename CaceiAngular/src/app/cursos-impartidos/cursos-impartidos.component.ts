import { Component, OnInit,ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import { CursosImpartidosService } from '../servicios/cursos-impartidos.service'; 

@Component({
  selector: 'app-cursos-impartidos',
  templateUrl: './cursos-impartidos.component.html',
  styleUrls: ['./cursos-impartidos.component.css']
})
export class CursosImpartidosComponent implements OnInit {

  modal;
  cursos_impartidos:JSON;
  claves_cursos: any = [];
  nombres_tipos_cursos: any = [];
  registros = null;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  columnas: string[] = ['id', 'curso impartido', 'accion']

  constructor(private modalService: NgbModal, private _service: CursosImpartidosService) { }

  ngOnInit(): void {
    this.llenarCatalogos();
  }

  borrarRegistro(element){
    this._service.eliminarCursosImpartidos(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  llenarCatalogos() {
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.claves_cursos = catalogos['claves_cursos'];
        this.nombres_tipos_cursos = catalogos['nombres_tipos_cursos'];
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator;
        console.log(catalogos['registros']);
      }
    );
  }

  agregarRegistro(content) {
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  cerrar(){
  	this.modal.close();
  }

  guardar(formulario:NgForm){
  	formulario.reset;
    this.cursos_impartidos = formulario.value;
    this.cursos_impartidos['id_usuario'] = 1;
    this._service.insertarCursosImpartidos(this.cursos_impartidos).subscribe (
      (respuesta: any) => {
        alert(respuesta['mensaje'])
      }
    )
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator;
        console.log(catalogos['registros']);
      }
    )
  	console.log(this.cursos_impartidos);
  	this.modal.close();
  }

}
