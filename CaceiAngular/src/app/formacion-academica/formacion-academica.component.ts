import { Component, OnInit, ViewChild  } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms'; 
import { MatTable, MatTableDataSource  } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import { FormacionAcademicaService } from './../servicios/formacion-academica.service';

@Component({
  selector: 'app-formacion-academica',
  templateUrl: './formacion-academica.component.html',
  styleUrls: ['./formacion-academica.component.css']
})
export class FormacionAcademicaComponent implements OnInit {

  modal;
  formacion:JSON;
  paises: any = [];
  nombres_tipos_formacion_academica: any = [];
  descripciones_tipos_localidad: any = [];
  registros = null;
  formacionEditar: JSON
  actualizar: boolean = false


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  displayedColumns: string[] = ['id','nombre','accion'];

  constructor(private modalService: NgbModal, private _service: FormacionAcademicaService) { }

  ngOnInit(): void {
    this.llenarCatalogos();
  }

  llenarCatalogos() {
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.paises = catalogos['paises'];
        this.nombres_tipos_formacion_academica = catalogos['nombres_tipos_formacion_academica'];
        this.descripciones_tipos_localidad = catalogos['descripciones_tipos_localidad'];
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator
        console.log(this.registros);
      }
    );
  }

  borrarRegistro(element){
    this._service.eliminarFormacionAcademica(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  agregarRegistro(content) {
    this.actualizar = false
    this.formacionEditar = JSON.parse("{}")
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  editarRegistro(content,element){
    this.actualizar = true
    this.formacionEditar = element
    //console.log(this.formacionEditar)
    this.modal = this.modalService.open(content, {size: 'lg'})
  }

  editar(formulario: NgForm){
    this.formacion = formulario.value
    this.formacion['id_formacion_academica'] = 
      this.formacionEditar['id_formacion_academica']
    //console.log(this.formacion)
    this.registros.data[this.registros.data.indexOf(this.formacionEditar)] 
      = this.formacion
    this.registros._updateChangeSubscription()
    //console.log(this.registros.data[0])
    this._service.actualizarFormacionAcademica(this.formacion).subscribe()

    this.cerrar()
  }

  cerrar(){
  	this.modal.close();
  }

  guardar(formulario:NgForm) {
  	formulario.reset;
  	this.formacion = formulario.value;
    this.formacion['id_usuario'] = 1;
    console.log(this.formacion);
    this._service.insertarFormacionAcademica(this.formacion).subscribe(
      (data: any) => {
        alert(data.mensaje);
      }
    );
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator
        console.log(this.registros);
      }
    )
  	this.modal.close();
  }

}
