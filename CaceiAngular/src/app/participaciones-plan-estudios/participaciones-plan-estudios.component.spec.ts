import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ParticipacionesPlanEstudiosComponent } from './participaciones-plan-estudios.component';

describe('ParticipacionesPlanEstudiosComponent', () => {
  let component: ParticipacionesPlanEstudiosComponent;
  let fixture: ComponentFixture<ParticipacionesPlanEstudiosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipacionesPlanEstudiosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipacionesPlanEstudiosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
