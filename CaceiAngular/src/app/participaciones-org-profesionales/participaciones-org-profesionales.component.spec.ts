import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ParticipacionesOrgProfesionalesComponent } from './participaciones-org-profesionales.component';

describe('ParticipacionesOrgProfesionalesComponent', () => {
  let component: ParticipacionesOrgProfesionalesComponent;
  let fixture: ComponentFixture<ParticipacionesOrgProfesionalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipacionesOrgProfesionalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipacionesOrgProfesionalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
