<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReconocimientosModel extends Model{
    protected $table = "reconocimientos";
    protected $primaryKey = "id_reconocimiento";
    const CREATED_AT = "fecha_registro";
    const UPDATED_AT = "fecha_modificacion";
    protected $fillable = [
    	"descripcion_reconocimiento",
    	"id_usuario"
    ];
    protected $visible = [
    	'id_reconocimiento',
    	'descripcion_reconocimiento'
    ];
}
