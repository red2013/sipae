<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParticipacionesPlanEstudiosModel extends Model{
    protected $table = "participaciones_plan_estudios";
    protected $primaryKey = "id_participacion_plan_estudios";
    const CREATED_AT = 'fecha_registro';
	const UPDATED_AT = 'fecha_modificacion';
	protected $fillable = [
		"descripcion_participacion_plan_estudios",
		"fecha_participacion_plan_estudios",
		"id_usuario"
	];
	protected $visible = [
		'id_participacion_plan_estudios',
		'descripcion_participacion_plan_estudios'
	];
}
