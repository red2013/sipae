<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExperienciaProfesionalModel extends Model{
    protected $table = "experiencias_profesionales";
    protected $primaryKey = "id_experiencias_profesionales";
    const CREATED_AT = "fecha_registro";
    const UPDATED_AT = "fecha_modificacion";
    protected $fillable = [
    	"actividad_puesto_experiencia",
    	"empresa_experiencia",
    	"fecha_inicio_experiencia",
    	"fecha_fin_experiencia",
    	"id_usuario"
    ];
    protected $visible = [
        "id_experiencias_profesionales",
        "actividad_puesto_experiencia"
    ];
}
