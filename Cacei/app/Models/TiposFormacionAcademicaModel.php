<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FormacionAcademicaModel;

class TiposFormacionAcademicaModel extends Model{
    protected $table = 'tipos_formacion_academica';
    protected $primaryKey = 'id_tipo_formacion_academica';
    public $timestamps = false;

    public function formacionesAcademicas(){
    	return $this->hasMany(FormacionAcademicaModel::class,
    		'id_tipo_formacion_academica','id_tipo_formacion_academica');
    }
}
