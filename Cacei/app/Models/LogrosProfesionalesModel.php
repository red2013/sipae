<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogrosProfesionalesModel extends Model{
    protected $table = "logros_profesionales";
    protected $primaryKey = "id_logro_profesional";
    const CREATED_AT = 'fecha_registro';
	const UPDATED_AT = 'fecha_modificacion';
    protected $fillable = [
    	"descripcion_logro_profesional",
    	"id_usuario"
    ];
    protected $visible = [
    	'id_logro_profesional',
    	'descripcion_logro_profesional'
    ];
}
