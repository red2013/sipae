<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PuestosAcademicosModel extends Model{
    protected $table = "puestos_academicos";
    protected $primaryKey = "id_puesto_academico";
    const CREATED_AT = "fecha_registro";
    const UPDATED_AT = "fecha_modificacion";
    protected $fillable = [
    	"actividad_puesto",
    	"institucion_puesto",
    	"fecha_inicio_puesto",
    	"fecha_fin_puesto",
    	"id_usuario"
    ];
    protected $visible = [
        "id_puesto_academico",
        "actividad_puesto"
    ];
}
