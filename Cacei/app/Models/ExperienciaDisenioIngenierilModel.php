<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\NivelesExperienciaIngenierilModel;

class ExperienciaDisenioIngenierilModel extends Model{
    protected $table = "experiencias_disenio_ingenieril";
    const CREATED_AT = "fecha_registro";
    const UPDATED_AT = "fecha_modificacion";
    protected $primaryKey = "id_experiencia_disenio_ingenieril";
    public $fillable = [
    	"organismo_exp_ingenieril",
    	"fecha_inicio_exp_ingenieril",
    	"fecha_fin_exp_ingenieril",
    	"id_nivel_experiencia_ingenieril",
    	"id_usuario"
    ];
    protected $visible = [
        "id_experiencia_disenio_ingenieril",
        "organismo_exp_ingenieril",
        "nombre_nivel_experiencia_ingenieril"
    ];

    public function nivelExperiencia(){
        return $this->belongsTo(NivelesExperienciaIngenierilModel::class,
            'id_nivel_experiencia_ingenieril');
    }
}
