<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginModel extends Model{
	protected $primaryKey = "id_usuario"; // define la primary key si no se llama id
	public $timestamps = false; // no tenemos definidas las comlumnas 
  protected $table = "usuarios"; // nombre de la tabla
  protected $fillable = [ // campos de la tabla
 		'nombre_usuario',
 		'contrasenia',
 		'fecha_registro',
 		'fecha_baja',
 		'id_tipo_usuario'
  ];
}
