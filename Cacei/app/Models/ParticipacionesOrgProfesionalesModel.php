<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\NivelesParticipacionOrgModel;

class ParticipacionesOrgProfesionalesModel extends Model{
    protected $table = "participaciones_org_profesionales";
    protected $primaryKey = "id_participacion_org_profesional";
    const CREATED_AT = 'fecha_registro';
	const UPDATED_AT = 'fecha_modificacion';
	protected $fillable = [
		"organismo_participacion",
		"fecha_inicio_participacion",
		"fecha_termino_participacion",
		"id_nivel_participacion_org",
		"id_usuario"
	];
	protected $visible = [
		'id_participacion_org_profesional',
		'organismo_participacion',
		'nombre_nivel_participacion_org',
		'fecha_inicio_participacion',
		'fecha_termino_participacion'
	];

	public function nivelParticipacionOrg(){
		return $this->belongsTo(NivelesParticipacionOrgModel::class,
			'id_nivel_participacion_org');
	}
}
