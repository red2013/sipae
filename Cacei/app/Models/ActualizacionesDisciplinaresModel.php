<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PaisesModel;

class ActualizacionesDisciplinaresModel extends Model{
    protected $table = "actualizaciones_disciplinares";
    protected $primaryKey = "id_actualizacion_disciplinar";
    const CREATED_AT = "fecha_registro";
    const UPDATED_AT = "fecha_modificacion";
    protected $fillable = [
    	"tipo_actualizacion",
    	"institucion_actualizacion",
    	"id_pais",
    	"fecha_actualizacion",
    	"horas_actualizacion",
    	"id_usuario"
    ];
    protected $visible = [
        "id_actualizacion_disciplinar",
        "tipo_actualizacion",
        "nombre_pais"
    ];

    public function pais(){
        return $this->belongsTo(PaisesModel::class,'id_pais');
    }
}
