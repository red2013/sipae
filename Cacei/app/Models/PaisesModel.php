<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CapacitacionesDocentesModel;
use App\Models\ActualizacionesDisciplinares;
use App\Models\FormacionAcademicaModel;

class PaisesModel extends Model{
    protected $table = 'paises';
    protected $primaryKey = 'id_pais';
    public $timestamps = false;

    public function capacitacionesDocentes(){
    	return $this->hasMany(CapacitacionesDocentesModel::class,'id_pais','id_pais');
    }

    public function actualizacionesDisciplinares(){
    	return $this->hasMany(ActualizacionesDisciplinares::class,'id_pais','id_pais');
    }

    public function formacionesAcademicas(){
    	return $this->hasMany(FormacionAcademicaModel::class,'id_pais','id_pais');
    }
}
