<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ClavesCursosModel;
use App\Models\TiposCursosModel;

class CursosImpartidosModel extends Model{
    protected $table = "cursos_impartidos";
    const CREATED_AT = "fecha_registro";
    const UPDATED_AT = "fecha_modificacion";
    protected $primaryKey = "id_cursos_impartidos";
    protected $fillable = [
    	"nombre_curso_impartido",
    	"fecha_inicio_curso_impartido",
    	"fecha_fin_curso_impartido",
    	"id_clave_curso",
    	"id_tipo_curso",
    	"horas_curso_impartido",
    	"evaluacion_desempenio",
    	"id_usuario"
    ];
    protected $visible = [
        'id_cursos_impartidos',
        'nombre_curso_impartido',
        'clave_curso',
        'nombre_tipo_curso'
    ];

    public function claveCurso(){
        return $this->belongsTo(ClavesCursosModel::class,'id_clave_curso');
    }
    public function tipoCurso(){
        return $this->belongsTo(TiposCursosModel::class,'id_tipo_curso');
    }
}
