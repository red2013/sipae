<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CursosImpartidosModel;

class ClavesCursosModel extends Model{
    protected $table = 'claves_cursos';
    protected $primaryKey = 'id_clave_curso';
    public $timestamps = false;

    public function cursosImpartidos(){
    	return $this->hasMany(CursosImpartidosModel::class,
    		'id_clave_curso','id_clave_curso');
    }
}
