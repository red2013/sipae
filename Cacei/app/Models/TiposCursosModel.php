<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CursosImpartidosModel;

class TiposCursosModel extends Model{
    protected $table = 'tipos_cursos';
    protected $primaryKey = 'id_tipo_curso';
    public $timestamps = false;

    public function cursosImpartidos(){
    	return $this->hasMany(CursosImpartidosModel::class,
    		'id_tipo_curso','id_tipo_curso');
    }
}
