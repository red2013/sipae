<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\ExportaRegistros;
use App\Models\FormacionAcademicaModel;
use App\Models\ActualizacionesDisciplinaresModel;
use App\Models\CapacitacionesDocentesModel;
use App\Models\ProductosAcademicosModel;
use App\Models\CursosImpartidosModel;
use App\Models\ExperienciaDisenioIngenierilModel;
use App\Models\ExperienciaProfesionalModel;
use App\Models\InformacionGeneralModel;
use App\Models\LogrosProfesionalesModel;
use App\Models\ParticipacionesOrgProfesionalesModel;
use App\Models\ParticipacionesPlanEstudiosModel;
use App\Models\PuestosAcademicosModel;
use App\Models\ReconocimientosModel;

!defined('FORMACION') && define('FORMACION','FORMACION ACADEMICA');
!defined('ACTUALIZACION') && define('ACTUALIZACION','ACTUALIZACION DISCIPLINAR');
!defined('CAPACITACION') && define('CAPACITACION','CAPACITACIONES DOCENTES');
!defined('PRODUCTO') && define('PRODUCTO','PRODUCTOS ACADEMICOS');
!defined('CURSO') && define('CURSO','CURSOS IMPARTIDOS');
!defined('EXPERIENCIA_INGENIERIL') && define('EXPERIENCIA_INGENIERIL','EXPERIENCIA INGENIERIL');
!defined('EXPERIENCIA_PROFESIONAL') && define('EXPERIENCIA_PROFESIONAL','EXPERIENCIA PROFESIONAL');
!defined('INFORMACION_GENERAL') && define('INFORMACION_GENERAL','INFORMACION GENERAL');
!defined('LOGROS') && define('LOGROS','LOGROS PROFESIONALES');
!defined('PARTICIPACIONES_ORG') && define('PARTICIPACIONES_ORG','PARTICIPACIONES ORGANIZACIONES');
!defined('PARTICIPACIONES_PLAN_ESTUDIOS') && define('PARTICIPACIONES_PLAN_ESTUDIOS','PARTICIPACIONES PLAN DE ESTUDIOS');
!defined('PUESTOS_ACADEMICOS') && define('PUESTOS_ACADEMICOS','PUESTOS ACADEMICOS');
!defined('RECONOCIMIENTOS') && define('RECONOCIMIENTOS','RECONOCIMIENTOS');

class ExportarDatos extends Controller{

    public function obtenerDatos(){
    	$titulos = [
    		[INFORMACION_GENERAL],
    		[FORMACION],
    		[ACTUALIZACION],
    		[CAPACITACION],
    		[PRODUCTO],
    		[CURSO],
    		[EXPERIENCIA_INGENIERIL],
    		[EXPERIENCIA_PROFESIONAL],
    		[LOGROS],
    		[PARTICIPACIONES_ORG],
    		[PARTICIPACIONES_PLAN_ESTUDIOS],
    		[PUESTOS_ACADEMICOS],
    		[RECONOCIMIENTOS]
    	];
    	$modelos = $this->llenarModelo($titulos);
    	return Excel::download(new ExportaRegistros($titulos,$modelos),"esp.xlsx");
    }

    private function llenarModelo($titulos){
    	$modelos = [];
    	foreach ($titulos as $titulo) {
    		switch($titulo[0]) {
    			case FORMACION:
    				array_push($modelos,$this->getFormaciones());
    				break;
    			case ACTUALIZACION:
    				array_push($modelos,$this->getActualizaciones());
    				break;
    			case CAPACITACION:
    				array_push($modelos,$this->getCapacitaciones());
    				break;
    			case PRODUCTO:
    				array_push($modelos,$this->getProductosAcademicos());
    				break;
    			case CURSO:
    				array_push($modelos,$this->getCursos());
    				break;
    			case EXPERIENCIA_INGENIERIL:
    				array_push($modelos,$this->getExperienciaIngenieril());
    				break;
    			case EXPERIENCIA_PROFESIONAL:
    				array_push($modelos,$this->getExperienciaProfesional());
    				break;
    			case INFORMACION_GENERAL:
    				array_push($modelos,$this->getInformacionGeneral());
    				break;
    			case LOGROS:
    				array_push($modelos,$this->getLogros());
    				break;
    			case PARTICIPACIONES_ORG:
    				array_push($modelos,$this->getParticipacionesOrg());
    				break;
    			case PARTICIPACIONES_PLAN_ESTUDIOS:
    				array_push($modelos,$this->getParticipacionesPlanEstudio());
    				break;
    			case PUESTOS_ACADEMICOS:
    				array_push($modelos,$this->getPuestosAcademicos());
    				break;
				case RECONOCIMIENTOS:
					array_push($modelos,$this->getReconocimientos());
					break;		
    			default:
    				break;
    		}
    	}
    	return $modelos;
    }

    private function getFormaciones(){
    	$formaciones = FormacionAcademicaModel::get();
    	foreach ($formaciones as $key => $registroFormacion) {
			$formaciones[$key]['nombre_tipo_formacion_academica'] 
				= $registroFormacion->tipoFormacion->nombre_tipo_formacion_academica;
			$formaciones[$key]['nombre_pais'] 
				= $registroFormacion->pais->nombre_pais;
			$formaciones[$key]['descripcion_tipo_localidad'] 
				= $registroFormacion->tipoLocalidad->descripcion_tipo_localidad;
		}
		return json_decode($formaciones,true);
    }

    private function getCapacitaciones(){
    	$capacitaciones = CapacitacionesDocentesModel::get();
        foreach ($capacitaciones as $key => $capacitacion) {
            $capacitaciones[$key]['nombre_pais'] = $capacitacion->pais->nombre_pais;
            $capacitaciones[$key]['nombre_nivel_capacitacion_docente'] = $capacitacion
                ->nivelCapacitacionDocente->nombre_nivel_capacitacion_docente;
        }
        return json_decode($capacitaciones,true);
    }

    private function getActualizaciones(){
    	$actualizaciones = ActualizacionesDisciplinaresModel::get();
		foreach ($actualizaciones as $key => $actualizacion) {
            $actualizaciones[$key]['nombre_pais'] = $actualizacion->pais->nombre_pais;
        }
        return json_decode($actualizaciones,true);
    }

    private function getProductosAcademicos(){
    	$productos = ProductosAcademicosModel::get();
    	return json_decode($productos,true);
    }

    private function getCursos(){
    	$cursos = CursosImpartidosModel::get();
        foreach ($cursos as $key => $curso) {
            $cursos[$key]['clave_curso'] = $curso->claveCurso->clave_curso;
            $cursos[$key]['nombre_tipo_curso'] = $curso->tipoCurso->nombre_tipo_curso;
        }
        return json_decode($cursos,true);
    }

   	private function getExperienciaIngenieril(){
   		$experiencias = ExperienciaDisenioIngenierilModel::get();
        foreach ($experiencias as $key => $experiencia) {
            $experiencias[$key]['nombre_nivel_experiencia_ingenieril'] 
                = $experiencia->nivelExperiencia->nombre_nivel_experiencia_ingenieril;
        }
        return json_decode($experiencias,true);
   	}

   	private function getExperienciaProfesional(){
   		$experiencias = ExperienciaProfesionalModel::get();
   		return json_decode($experiencias,true);
   	}

   	private function getInformacionGeneral(){
   		$inforamcion = InformacionGeneralModel::get();
   		return json_decode($inforamcion,true);
   	}

   	private function getLogros(){
   		$logros = LogrosProfesionalesModel::get();
   		return json_decode($logros,true);
   	}

   	private function getParticipacionesOrg(){
   		$participaciones = ParticipacionesOrgProfesionalesModel::get();
        foreach ($participaciones as $key => $participacion) {
            $participaciones[$key]['nombre_nivel_participacion_org'] =
                $participacion->nivelParticipacionOrg->nombre_nivel_participacion_org;
        }
        return json_decode($participaciones,true);
   	}

   	private function getParticipacionesPlanEstudio(){
   		$participaciones = ParticipacionesPlanEstudiosModel::get();
   		return json_decode($participaciones,true);
   	}

   	private function getPuestosAcademicos(){
   		$puestos = PuestosAcademicosModel::get();
   		return json_decode($puestos,true);
   	}

   	private function getReconocimientos(){
   		$reconocimientos = ReconocimientosModel::get();
   		return json_decode($reconocimientos,true);
   	}
}
