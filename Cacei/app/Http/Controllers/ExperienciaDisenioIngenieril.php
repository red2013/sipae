<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ExperienciaDisenioIngenierilModel;
use App\Models\NivelesExperienciaIngenierilModel;
use Validator;

class ExperienciaDisenioIngenieril extends Controller{

    public function obtenerCatalogos(){
    	$nombres_niveles_experiencia_ingenieril = NivelesExperienciaIngenierilModel::
            pluck("nombre_nivel_experiencia_ingenieril");
        $experiencias = ExperienciaDisenioIngenierilModel::get();
        foreach ($experiencias as $key => $experiencia) {
            $experiencias[$key]['nombre_nivel_experiencia_ingenieril'] 
                = $experiencia->nivelExperiencia->nombre_nivel_experiencia_ingenieril;
        }
    	$respuesta = [
    		"nombres_niveles_experiencia_ingenieril" 
    			=> $nombres_niveles_experiencia_ingenieril,
            "registros" => $experiencias
    	];
    	return response()->json($respuesta,200);
    }
    public function insertarExperienciaDisenioIngenieril(Request $req){
    	$reglas = [
    		"organismo_exp_ingenieril" => "required",
	    	"fecha_inicio_exp_ingenieril" => "required",
	    	"fecha_fin_exp_ingenieril" => "required",
	    	"nombre_nivel_experiencia_ingenieril" => "required",
	    	"id_usuario" => "required"
    	];
        $experiencia = $req->json()->all();
    	$validacion = Validator::make($experiencia,$reglas);
    	if($validacion->fails()){
    		return response()->json(["mensaje" => "Error al subir la información",
                "error" => $validacion->errors()],400);
    	}
        $experiencia['id_nivel_experiencia_ingenieril'] = 
            DB::table('niveles_experiencia_ingenieril')
            ->where("nombre_nivel_experiencia_ingenieril",
                $experiencia['nombre_nivel_experiencia_ingenieril'])
            ->value('id_nivel_experiencia_ingenieril');
    	$datos = ExperienciaDisenioIngenierilModel::create($experiencia);
        $respuesta = [
            "mensaje" => "Se registro correctamente"
        ];
    	return response()->json($respuesta,201); 
    }

    public function eliminarExperienciaDisenioIngenieril(Request $req,
        ExperienciaDisenioIngenierilModel $experiencia){
        $experiencia->delete();
        return response()->json(null,204);
    }

    public function actualizarExperienciaDisenioIngenieril(Request $req, 
        ExperienciaDisenioIngenierilModel $experiencia){
        $datos = $req->json()->all();
        $nivel = NivelesExperienciaIngenierilModel::
        where('nombre_nivel_experiencia_ingenieril',
            $datos['nombre_nivel_experiencia_ingenieril'])->first();
        $datos['id_nivel_experiencia_ingenieril'] = $nivel->id_nivel_experiencia_ingenieril;
        $experiencia->update($datos);
        return response()->json($experiencia,200);
    }
}
