<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LoginModel;
use Illuminate\Support\Facades\Crypt;

class Login extends Controller{

    public function login(Request $req){
    	$datos = $req->json()->all();
        $usuario = LoginModel::
            where('nombre_usuario',$datos['correo'])
            ->where('contrasenia',$datos['contrasenia'])
            ->first();
        if(is_null($usuario)){
            $respuesta = [
                "mensaje"=> "Usuario o contraseña incorrectos",
            ];
            return response()->json($respuesta,404);
        }
        $usuario['logged'] = true;
        $usuario['token'] = Crypt::encryptString(bin2hex(random_bytes(64)));
        $req->session()->put("usuario",$usuario);
        return response()->json($usuario,200);
    }

    public function verificarSesion(Request $req){
        /*$respuesta = $req->session()->get("usuario"); //temporalmente fuera de servicio por diferentes dominios
        if(is_null($respuesta)){
            return response()->json(["mensaje" => "No ha iniciado sesion"],400);
        }*/
        if(!$req->json()->all()['logged']){
          return response()->json(["mensaje" => "error"],400);  
        }
        $respuesta = [
            "logged" => true
        ];
        return response()->json($respuesta,200);
    }

    public function logOut(Request $req){
        $req->session()->forget("usuario");
        return response()->json(["mensaje" => "sesion finalizada"],200);
    }

}
