<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InformacionGeneralModel;
use Validator;

class InformacionGeneral extends Controller{
    public function informacionGeneral(Request $req){
    	$reglas = [
    		"numero_profesor" => "required",
	    	"nombres_profesor" => "required",
	    	"apellido_paterno" => "required",
	    	"apellido_materno" => "required",
	    	"fecha_nacimiento" => "required",
	    	"puesto_en_institucion" => "required",
	    	"fecha_contratacion_institucion" => "required",
	    	"categoria_contratacion_actual" => "required",
	    	"tipo_contratacion_actual" => "required",
	    	"id_usuario" => "required"
    	];
    	$validacion = Validator::make($req->json()->all(),$reglas);
    	if($validacion->fails()){
    		return response()->json("Error al cargar la información",400);
    	}
    	$datos = InformacionGeneralModel::create($req->json()->all());
    	return response()->json("Informacion agregada correctamente",201);
    }
}
