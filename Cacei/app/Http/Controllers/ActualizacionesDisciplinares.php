<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\ActualizacionesDisciplinaresModel;
use App\Models\PaisesModel;

class ActualizacionesDisciplinares extends Controller{
    
    public function obtenerCatalogos(){
    	$paises = PaisesModel::pluck('nombre_pais');
		$actualizaciones = ActualizacionesDisciplinaresModel::get();
        foreach ($actualizaciones as $key => $actualizacion) {
            $actualizaciones[$key]['nombre_pais'] = $actualizacion->pais->nombre_pais;
        }
		$respuesta = [
			"paises" => $paises,
            "registros" => $actualizaciones
		];
		return response()->json($respuesta,200);
    }

    public function insertarActualizacion(Request $req){
    	$reglas = [
	    	"tipo_actualizacion" => "required",
	    	"institucion_actualizacion" => "required",
	    	"nombre_pais" => "required",
	    	"fecha_actualizacion" => "required",
	    	"horas_actualizacion" => "required",
	    	"id_usuario" => "required"
    	];
        $actualizacion = $req->json()->all();
        $validacion = Validator::make($actualizacion,$reglas);
        if($validacion->fails()){
            return response()->json(["mensaje" => "Error al cargar la información"],400);
        }
        $actualizacion['id_pais'] = DB::table('paises')->where('nombre_pais',$actualizacion['nombre_pais'])
            ->value('id_pais');
    	$datos = ActualizacionesDisciplinaresModel::create($actualizacion);
        $respuesta = [
            "mensaje" => "Se registro correctamente"
        ];
    	return response()->json($respuesta,201);
    }

    public function eliminarActualizacion(Request $req,
        ActualizacionesDisciplinaresModel $actualizacion){
        $actualizacion->delete();
        return response()->json(null,204);
    }

    public function actualizarActualizacion(Request $req,
        ActualizacionesDisciplinaresModel $actualizacion){
        $datos = $req->json()->all();
        $pais = PaisesModel::where('nombre_pais',$datos['nombre_pais'])->first();
        $datos['id_pais'] = $pais->id_pais;
        $actualizacion->update($datos);
        return response()->json($actualizacion,200);
    }

}
