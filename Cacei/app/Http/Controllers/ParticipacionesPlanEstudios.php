<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ParticipacionesPlanEstudiosModel;
use Validator;

class ParticipacionesPlanEstudios extends Controller{

    public function obtenerCatalogos(){
        $participaciones = ParticipacionesPlanEstudiosModel::get();
        $respuesta = [
            "registros" => $participaciones
        ];
        return response()->json($respuesta,200);
    }

    public function insertarParticipacionPlanEstudios(Request $req){
    	$reglas = [
    		"descripcion_participacion_plan_estudios" => "required",
			"fecha_participacion_plan_estudios" => "required",
			"id_usuario" => "required"
    	];
    	$validacion = Validator::make($req->json()->all(),$reglas);
    	if($validacion->fails()){
    		return response()->json(["mensaje" => "Error al cargar la informacion"],400);
    	}
    	$datos = ParticipacionesPlanEstudiosModel::create($req->json()->all());
        $respuesta = [
            "mensaje" => "Se agregó correctamente"
        ];
    	return response()->json($respuesta,201);
    }

    public function eliminarParticipacionPlanEstudios(Request $req, 
        ParticipacionesPlanEstudiosModel $participacion){
        $participacion->delete();
        return response()->json(null,204);
    }

    public function actualizarParticipacionPlanEstudios(Request $req, 
        ParticipacionesPlanEstudiosModel $participacion){
        $participacion->update($req->json()->all());
        return response()->json($participacion,200);
    }
}
