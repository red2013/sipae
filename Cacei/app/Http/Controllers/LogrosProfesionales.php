<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LogrosProfesionalesModel;
use Validator;

class LogrosProfesionales extends Controller{

    public function obtenerCatalogos(){
        $logros = LogrosProfesionalesModel::get();
        $respuesta = [
            "registros" => $logros
        ];
        return response()->json($respuesta,200);
    }

    public function insertarLogroProfesional(Request $req){
    	$reglas = [
    		"descripcion_logro_profesional" => "required",
    		"id_usuario" => "required"
    	];
    	$validacion = Validator::make($req->json()->all(),$reglas);
    	if($validacion->fails()){
    		return response()->json(["mensaje" => "Error al cargar la información"],400);
    	}
    	$datos = LogrosProfesionalesModel::create($req->json()->all());
        $respuesta = [
            "mensaje" => "Se registro correctamente"
        ];
    	return response()->json($respuesta,201);
    }

    public function eliminarLogroProfesional(Request $req, 
        LogrosProfesionalesModel $logro){
        $logro->delete();
        return response()->json(null,204);
    }

    public function actualizarLogroProfesional(Request $req, 
        LogrosProfesionalesModel $logro){
        $logro->update($req->json()->all());
        return response()->json($logro,200);
    }
}
