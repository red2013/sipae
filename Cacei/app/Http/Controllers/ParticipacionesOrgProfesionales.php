<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ParticipacionesOrgProfesionalesModel;
use App\Models\NivelesParticipacionOrgModel;
use Validator;

class ParticipacionesOrgProfesionales extends Controller{

	public function obtenerCatalogos(){
		$nivel_participacion_org = 
            NivelesParticipacionOrgModel::pluck("nombre_nivel_participacion_org");
        $participaciones = ParticipacionesOrgProfesionalesModel::get();
        foreach ($participaciones as $key => $participacion) {
            $participaciones[$key]['nombre_nivel_participacion_org'] =
            $participacion->nivelParticipacionOrg->nombre_nivel_participacion_org;
        }
		$respuesta = [
			"niveles_participacion_org" => $nivel_participacion_org,
            "registros" => $participaciones
		];
		return response()->json($respuesta,200);
	}

    public function insertarParticipacionOrgProfesional(Request $req){
    	$reglas = [
    		"organismo_participacion" => "required",
			"fecha_inicio_participacion" => "required",
			"fecha_termino_participacion" => "required",
			"nombre_nivel_participacion_org" => "required",
			"id_usuario" => "required"
    	];
    	$participacion = $req->json()->all();
    	$validacion = Validator::make($participacion,$reglas);
    	if($validacion->fails()){
    		return response()->json(["mensaje" => "Error al cargar la información"],400);
    	}
    	$participacion['id_nivel_participacion_org'] = 
    		DB::table('niveles_participacion_org')
    		->where('nombre_nivel_participacion_org',
    			$participacion['nombre_nivel_participacion_org'])
    		->value('id_nivel_participacion_org');
    	$datos = ParticipacionesOrgProfesionalesModel::create($participacion);
        $respuesta = [
            "mensaje" => "Se insertó correctamente"
        ];
    	return response()->json($respuesta,201);
    }

    public function eliminarParticipacionOrgProfesional(Request $req, 
        ParticipacionesOrgProfesionalesModel $participacion){
        $participacion->delete();
        return response()->json(null,204);
    }

    public function actualizarParticipacionOrgProfesional(Request $req, 
        ParticipacionesOrgProfesionalesModel $participacion){
        $datos = $req->json()->all();
        $nivel = NivelesParticipacionOrgModel::where("nombre_nivel_participacion_org", 
            $datos['nombre_nivel_participacion_org'])->first();
        $datos['id_nivel_participacion_org'] = $nivel->id_nivel_participacion_org;
        $participacion->update($datos);
        return response()->json($participacion,200);
    }
}
