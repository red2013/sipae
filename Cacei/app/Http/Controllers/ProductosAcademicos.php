<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductosAcademicosModel;
use Validator;

class ProductosAcademicos extends Controller{

    public function obtenerCatalogos(){
        $productos = ProductosAcademicosModel::get();
        $respuesta = [
            "registros" => $productos
        ];
        return response()->json($respuesta,200);
    }

    public function insertarProductoAcademico(Request $req){
    	$reglas = [
    		"numero_producto_academico" => "required",
	    	"descripcion_prod_academico" => "required",
	    	"id_usuario" => "required"
    	];
    	$validacion = Validator::make($req->json()->all(),$reglas);
    	if($validacion->fails()){
    		return response()->json(["mensaje" => "Error al cargar la información"],400);
    	}
    	$datos = ProductosAcademicosModel::create($req->json()->all());
    	$respuesta = [
            "mensaje" => "Se registro correctamente"
        ];
        return response()->json($respuesta,201);
    }

    public function eliminarProductoAcademico(Request $req, 
        ProductosAcademicosModel $producto){
        $producto->delete();
        return response()->json(null,204);
    }

    public function actualizarProductoAcademico(Request $req, 
        ProductosAcademicosModel $producto){
        $producto->update($req->json()->all());
        return response()->json($producto,200);
    }
}
