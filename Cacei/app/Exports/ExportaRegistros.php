<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportaRegistros implements 
	FromCollection, ShouldAutoSize, WithCustomStartCell, WithEvents{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $filas = [2];
    private $letras = [];
    private $columnas = [];
    private $titulos;
    private $modelos;
    
    function __construct($titulos,$modelos){
    	$this->titulos = $titulos;
    	$this->modelos = $modelos;
    	$this->llenarColumnas();
    	$this->contarFilas();
    }

    private function llenarColumnas(){
    	foreach (range('A', 'Z') as $char) {
    		array_push($this->letras, $char);
		}
    }

    private function contarFilas(){
    	if (!defined('ITABLA')) define('ITABLA', 4);
    	$i = 0;
    	foreach ($this->modelos as $modelo) {
    		//print_r($modelo);
    		array_push($this->filas,count($modelo)+end($this->filas)+ITABLA);
    		$i++;
    	}
    }

    public function collection(){
    	$excel = [];
    	$i = 0;
    	foreach ($this->modelos as $key => $tabla) {
    		$excel[] = $this->titulos[$i];
    		$llaves = array_keys($tabla[0]);
    		$excel[] = $llaves;
    		array_push($this->columnas,count($llaves));
    		foreach ($tabla as $fila) {
    			$excel[] = array_values($fila);
    		}
    		$excel[] = ['']; // saltos de linea
    		$excel[] = [''];
    		$i++;
    	}
    	$coleccion = collect($excel);
        return $coleccion;
    }

    public function registerEvents(): array{
    	//estilos
    	$titulo = [
    		'alignment' => [
		        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
		    ],
		    'font' => [
				'bold' =>true
			],
			'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '4177f4',
                 ]           
            ],
            'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
				]
			],
    	];
    	$encabezados = [
			'font' => [
				'bold' =>true
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
				]
			],
			'alignment' => [
		        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
		    ],
		    'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => 'bcbec5',
                 ]           
            ],
		];
		$registros = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
				]
			],
			'alignment' => [
		        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
		    ],
		];
		if (!defined('IENCABEZADO')) define('IENCABEZADO', 1); //salto hacia encabezado
		if (!defined('IREGISTRO')) define('IREGISTRO', 2); //salto hacia registros
		/*
		+---------------------------------....-----------+
		|					Titulo						 | 0
		+----------+----------+----------+....+----------+
		|encabezad1|encabezad2|encabezad3|....|encabezadN| 1
		+----------+----------+----------+....+----------+
		|registro11|registro12|registro13|....|registro1N| 2
		+----------+----------+----------+....+----------+
		|registro21|registro22|registro23|....|registro2N|
		+----------+----------+----------+....+----------+
		|registro31|registro32|registro33|....|registro3N|
		+----------+----------+----------+....+----------+
		..................................................
		..................................................
		..................................................
		..................................................
		+----------+----------+----------+....+----------+
		|registroN1|registroN2|registroN3|....|registroNN|
		+----------+----------+----------+....+----------+

		*/
    	return [
    		AfterSheet::class => function(AfterSheet $event) 
    			use ($encabezados,$registros,$titulo){
    			for ($i=0; $i <count($this->filas)-1 ; $i++) { 
    				$event->sheet
    					->getStyle('B'.($this->filas[$i]+IENCABEZADO).':'.
    						$this->letras[$this->columnas[$i]].($this->filas[$i]+IENCABEZADO))
    					->applyFromArray($encabezados);
    				$event->sheet
    					->getStyle('B'.($this->filas[$i]+IREGISTRO).':'.
    						$this->letras[$this->columnas[$i]].($this->filas[$i+1]-3))
    					->applyFromArray($registros); // 
    				$event->sheet
    					->getDelegate()->mergeCells('B'.$this->filas[$i].':'.
    						$this->letras[$this->columnas[$i]].$this->filas[$i]);
    				$event->sheet
    					->getStyle('B'.$this->filas[$i].':'.
    						$this->letras[$this->columnas[$i]].$this->filas[$i])
    					->applyFromArray($titulo);
    				/*$event->sheet->getDelegate()->getStyle('B'.$this->filas[$i].':'.
    						$this->letras[$this->columnas[$i]].$this->filas[$i])
    					->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);*/
    			}
    		}
    	];
    }

	public function startCell(): string{
	    return 'B2';
	}    
}
